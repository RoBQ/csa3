"""Модуль интерфейса инструкций"""

import json
from enum import Enum
from typing import Union, TypedDict

import numpy as np


class Opcode(str, Enum):
    ADD = 'add'
    SUB = 'sub'
    MOD = 'mod'
    INC = 'inc'
    DEC = 'dec'
    LD = 'ld'
    ST = 'st'
    MV = 'mv'
    BE = 'be'
    BNE = 'bne'
    JMP = 'jmp'
    HLT = 'hlt'
    IN = 'in'
    OUT = 'out'


class Instr(TypedDict):
    opcode: Opcode
    args: list[int | str]


Cell = Union[np.int32, Instr]


def write_code(filename: str, code: list[Cell]):
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))


def read_code(filename: str) -> list[Cell]:
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())
    for i, cell in enumerate(code):
        if isinstance(cell, int):
            code[i] = np.int32(cell)
        elif isinstance(cell, Opcode):
            cell["opcode"] = Opcode(cell["opcode"])

    return code
