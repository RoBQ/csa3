# АК. Лабораторная работа №3
### Бавыкина Романа Алексеевича P33101
### alg | risc | neum | hw | instr | struct | stream | port | prob1

* alg -- синтаксис языка должен напоминать java/javascript/luа. 
Должен поддерживать математические выражения.
* risc -- система команд должна быть упрощенной, в духе RISC архитектур.
* neum -- фон Неймановская архитектура.
* hw -- Control Unit - hardwired. Реализуется как часть модели.
* instr -- процессор необходимо моделировать с точностью до каждой инструкции
(наблюдается состояние после каждой инструкции).
* struct -- Представление машинного кода в виде высокоуровневой структуры данных. Считается, что одна инструкция 
укладывается в одно машинное слово, за исключением CISC архитектур.
* stream -- Ввод-вывод осуществляется как поток токенов.
* port -- Ввод-вывод ISA port-mapped
* prob1 -- If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.

## Язык программирования

``` ebnf
program ::= statement | {statement ";"} statement

statement ::= assign_statement
            | if_statement
            | while_statement
            | print_statement
            | print_var_statement
            | scan_statement
            
assign_statement    ::= id "=" a_expr
if_statement        ::= "if" b_expr "{" {statement} "}"
while_statement     ::= "while" b_expr "{" {statement} "}"
print_statement     ::= "print" string
print_var_statement ::= "print_var" id
scan_statement      ::= "scan" id

a_expr ::= (int | id) | ((int | id) a_operator (int | id))
b_expr ::= a_expr b_operator a_expr

int ::= [0-9]+
id ::= [A-Za-z_][A-Za-z0-9_]*
string ::= \".*\"

a_operator ::= "+" | "-" | "%"
b_operator ::= "==" | "!="

```

Код выполняется последовательно. Возможности:

- Объявление переменных и простые арифметические операции над ними: `a = 1; a = a + 2`. Трансляция сложных
арифметических операций не реализована вследствие отсутствия необходимости для выполнения алгоритма по заданному варианту.
- Цикл: `while <simple_condition> { ... }`.
- Условия: `if <simple_condition> { ... }`
- Операторы вводы-вывода: `print <string>` (позволяет вывести строковый литерал),  `print_var <id>` (позволяет вывести значение переменной), `scan <id>` (читает из входного буфера).

Пример программы на языке:

``` c
sum = 0;
i = 1;

while i != 1000 {
    if i % 3 == 0 {
        sum = sum + i
    };
    if i % 5 == 0 {
        sum = sum + i
    };
    if i % 15 == 0 {
        sum = sum - i
    };
    i = i + 1
};
print_var sum
```

## Организация памяти

Модель памяти процессора:
- Общая память программ и данных. Машинное слово -- 32 бита, знаковое. Реализуется списком данных типа Cell = Instr | np.int32, Instr - словарь, описывающий инструкцию, содержит опкод и список аргументов.

## Система команд

- Машинное слово - 32 бита, знаковое.
- Память:
    - Доступ к памяти осуществляется через инструкции LD и ST.
    - Может быть записана из регистров общего назначения: R0-R3.
    - Может быть прочитана в регистры общего назначения: R0-R3.
- АЛУ:
    - Левый вход АЛУ соединен с шиной 1 через мультиплексор, он позволяет передать значение из инструкции вместо регистров, используется при переходах.
    - Правый вход АЛУ соединен с шиной 2, на шину можно подать любой регистр.
    - АЛУ поддерживает операции сложения и деление по модулю.
    - Входящее в АЛУ слово может быть инвертировано или инкреминтировано, это контролируется с помошью сигналов, за счет этого реализуется операция вычитания.
    - Чтение из памяти и устройств ввода вывода происходит в АЛУ
- Регистры:
    - сигнал reg_select позволяет выбрать, что пойдет на шину 1 и шину 2
    - Четыре регистра общего назначения R0-R3
    - Регистр R5 (PC) -  счетчик команд

## Набор инструкций

| Синтаксис                  | Количество тактов | Комментарий
|:---------------------------|:------------------|:------------------------------------------------------------------
| `add reg_1 reg_2 reg_res`  | 2                 | сложить `reg_1` и `reg_2`, результат в `reg_res`
| `sub reg_1 reg_2 reg_res`  | 2                 | вычесть из `reg_1` `reg_2`, результат в `reg_res`
| `mod reg_1 reg_2 reg_res`  | 2                 | взять остаток от деления `reg_1` на `reg_2`, результат в `reg_res`
| `inc reg reg_res`          | 2                 | инкрементировать `reg` результат в `reg_res`
| `dec reg reg_res`          | 2                 | декрементировать `reg` результат в `reg_res`
| `ld mem_addr reg`          | 2                 | загрузить значение из `mem_addr` в `reg`
| `st reg mem_addr`          | 2                 | сохранить значение из `reg` в `mem_addr`
| `mv reg_1 reg_2`           | 2                 | скопировать значение из `reg_1` в `reg_2`
| `be addr`                  | 2-3               | если `Z = 0`, переход на `addr`
| `bne addr`                 | 2-3               | если `Z = 1`, переход на `addr`
| `jmp addr`                 | 2                 | переход на `addr`
| `in reg`                   | 2                 | прочитать значение с устройства в `reg`
| `out reg`                  | 2                 | записать значение в устройство из `reg`
| `hlt`                      | 1                 | остановить процессор

## Кодирование инструкций

- Машинный код сериализуется в список JSON

Пример:
``` json
[
      {
          "opcode": "ld",
          "args": [
              10,
              1
          ]
      },
      {
          "opcode": "ld",
          "args": [
              11,
              2
          ]
      },
      {
          "opcode": "sub",
          "args": [
              1,
              2,
              0
          ]
      },
      {
          "opcode": "bne",
          "args": [
              9
          ]
      },
      {
          "opcode": "in",
          "args": [
              0
          ]
      },
      {
          "opcode": "st",
          "args": [
              0,
              12
          ]
      },
      {
          "opcode": "ld",
          "args": [
              12,
              0
          ]
      },
      {
          "opcode": "out",
          "args": [
              0
          ]
      },
      {
          "opcode": "jmp",
          "args": [
              0
          ]
      },
      {
          "opcode": "hlt"
      },
      0,
      0,
      ""
  ]
```

- `opcode` -- опкод операции
- `args` -- массив аргументов операции

## Транслятор

Интерфейс командной строки: `translator.py <input_file_name> <(Optional) output_file_name>`

Этапы трансляции:
1. Траснсформирование текста в последовательность значимых термов.
2. Формирование абстрактного синтаксического дерева.
3. Генерация машинного кода.
4. Доабавление к коду данных и линковка

## Модель процессора
![image](img/scheme.jpg)
### DataPath
Реализован в классе `DataPath`.

- `memory` -- однопортовая, поэтому либо читаем, либо пишем;
- `input_buffer` -- вызовет остановку процесса моделирования, если буфер входных значений закончился;
- `_bus1` - шина 1, соединяется с мультиплексором выбора константы и идет в АЛУ;
- `_bus2` - шина 2, идет в АЛУ и в память;
- `_bus1_mux` - шина, выходящая из мультиплексора выбора константы, данные из нее ведут в АЛУ;
- `alu` - выходная шина АЛУ, соединяется с шиной адреса памяти и ведет к регистрам;
- `zero` - zero флаг АЛУ, передается на CU по сигналу, используется для условных переходов

### ControlUnit

Реализован в классе `ControlUnit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность сигналов: `decode_and_execute_instruction`.

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging.
- Количество инструкций для моделирования ограничено hardcoded константой.
- Остановка моделирования осуществляется при помощи исключения: `StopIteration`


## Апробация

В качестве тестов использовано три алгоритма:

1. examples/hello.cmm -- Hello world
2. examples/cat.cmm -- программа `cat`, повторяем ввод на выводе.
3. examples/prob1.cmm -- Первая проблема Эйлера

Golden тесты: tests.py, golden/*.yml

CI:

```yaml
lab3-test:
  stage: test
  image:
    name: python-tools
    entrypoint: [""]
  script:
    - pip install pytest-golden numpy
    - python3-coverage run -m pytest --verbose tests.py
    - find . -type f -name "*.py" | xargs -t python3-coverage report
    - find . -type f -name "*.py" | xargs -t pep8 --ignore=E501
    - find . -type f -name "*.py" | xargs -t pylint --disable C0115,C0301,R0903,W0613,W0108,W0621,W0614,C0114,R0902,R0912,W4901,W0401,C0209,R0915,W0402,R0201

```

Журнал работы ппроцессора на примере cat:
```console
DEBUG    root:machine.py:343 {TICK: 0, R4(PC): 0, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 2, R4(PC): 1, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 4, R4(PC): 2, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 6, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 8, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 10, R4(PC): 5, R0: !, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 12, R4(PC): 6, R0: !, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 14, R4(PC): 7, R0: !, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 16, R4(PC): 8, R0: !, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 18, R4(PC): 0, R0: !, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 20, R4(PC): 1, R0: !, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 22, R4(PC): 2, R0: !, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 24, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 26, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 28, R4(PC): 5, R0: d, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 30, R4(PC): 6, R0: d, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 32, R4(PC): 7, R0: d, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 34, R4(PC): 8, R0: d, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 36, R4(PC): 0, R0: d, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 38, R4(PC): 1, R0: d, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 40, R4(PC): 2, R0: d, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 42, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 44, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 46, R4(PC): 5, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 48, R4(PC): 6, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 50, R4(PC): 7, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 52, R4(PC): 8, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 54, R4(PC): 0, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 56, R4(PC): 1, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 58, R4(PC): 2, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 60, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 62, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 64, R4(PC): 5, R0: r, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 66, R4(PC): 6, R0: r, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 68, R4(PC): 7, R0: r, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 70, R4(PC): 8, R0: r, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 72, R4(PC): 0, R0: r, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 74, R4(PC): 1, R0: r, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 76, R4(PC): 2, R0: r, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 78, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 80, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 82, R4(PC): 5, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 84, R4(PC): 6, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 86, R4(PC): 7, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 88, R4(PC): 8, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 90, R4(PC): 0, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 92, R4(PC): 1, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 94, R4(PC): 2, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 96, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 98, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 100, R4(PC): 5, R0: w, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 102, R4(PC): 6, R0: w, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 104, R4(PC): 7, R0: w, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 106, R4(PC): 8, R0: w, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 108, R4(PC): 0, R0: w, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 110, R4(PC): 1, R0: w, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 112, R4(PC): 2, R0: w, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 114, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 116, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 118, R4(PC): 5, R0:  , R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 120, R4(PC): 6, R0:  , R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 122, R4(PC): 7, R0:  , R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 124, R4(PC): 8, R0:  , R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 126, R4(PC): 0, R0:  , R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 128, R4(PC): 1, R0:  , R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 130, R4(PC): 2, R0:  , R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 132, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 134, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 136, R4(PC): 5, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 138, R4(PC): 6, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 140, R4(PC): 7, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 142, R4(PC): 8, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 144, R4(PC): 0, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 146, R4(PC): 1, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 148, R4(PC): 2, R0: o, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 150, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 152, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 154, R4(PC): 5, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 156, R4(PC): 6, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 158, R4(PC): 7, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 160, R4(PC): 8, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 162, R4(PC): 0, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 164, R4(PC): 1, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 166, R4(PC): 2, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 168, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 170, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 172, R4(PC): 5, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 174, R4(PC): 6, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 176, R4(PC): 7, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 178, R4(PC): 8, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 180, R4(PC): 0, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 182, R4(PC): 1, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 184, R4(PC): 2, R0: l, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 186, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 188, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 190, R4(PC): 5, R0: e, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 192, R4(PC): 6, R0: e, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 194, R4(PC): 7, R0: e, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 196, R4(PC): 8, R0: e, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 198, R4(PC): 0, R0: e, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 200, R4(PC): 1, R0: e, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 202, R4(PC): 2, R0: e, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 204, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 206, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 208, R4(PC): 5, R0: H, R1: 0, R2: 0, R3: 0} {'opcode': 'st', 'args': [0, 12]}
  DEBUG    root:machine.py:349 {TICK: 210, R4(PC): 6, R0: H, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [12, 0]}
  DEBUG    root:machine.py:349 {TICK: 212, R4(PC): 7, R0: H, R1: 0, R2: 0, R3: 0} {'opcode': 'out', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 214, R4(PC): 8, R0: H, R1: 0, R2: 0, R3: 0} {'opcode': 'jmp', 'args': [0]}
  DEBUG    root:machine.py:349 {TICK: 216, R4(PC): 0, R0: H, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [10, 1]}
  DEBUG    root:machine.py:349 {TICK: 218, R4(PC): 1, R0: H, R1: 0, R2: 0, R3: 0} {'opcode': 'ld', 'args': [11, 2]}
  DEBUG    root:machine.py:349 {TICK: 220, R4(PC): 2, R0: H, R1: 0, R2: 0, R3: 0} {'opcode': 'sub', 'args': [1, 2, 0]}
  DEBUG    root:machine.py:349 {TICK: 222, R4(PC): 3, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'bne', 'args': [9]}
  DEBUG    root:machine.py:349 {TICK: 224, R4(PC): 4, R0: 0, R1: 0, R2: 0, R3: 0} {'opcode': 'in', 'args': [0]}
  DEBUG    root:machine.py:146 input buffer is empty
  INFO     root:machine.py:374 instr_counter: 112, ticks: 225
  INFO     root:machine.py:378 ['!', 'd', 'l', 'r', 'o', 'w', ' ', 'o', 'l', 'l', 'e', 'H']
```
| ФИО                      | алг.  | LoC   | code байт | code инстр. | инстр. | такт.  | варинт
|:-------------------------|:------|:------|:----------|:------------|:-------|:-------|:--------------------------------------------------------------------
| Бавыкин Роман Алексеевич | hello | 1     | 2036      | 23          | 22     | 45     | alg | risc | neum | hw | instr | struct | stream | port | prob1
| Бавыкин Роман Алексеевич | cat   | 5     | 870       | 10          | 112    | 225    | alg | risc | neum | hw | instr | struct | stream | port | prob1
| Бавыкин Роман Алексеевич | prob1 | 16    | 4072      | 42          | 29371  | 61143  | alg | risc | neum | hw | instr | struct | stream | port | prob1